package com.firemounta1n.alfabanktest;

import android.content.res.Configuration;
import android.os.Bundle;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    public static final String mBroadcastReceiveAction = "com.firemounta1n.alfabanktest.receive";

    private String[] titles;
    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private IntentFilter mIntentFilter;
    private ActionBarDrawerToggle drawerToggle;
    private int currentPosition = 0;
    private int mNavItemId;

    private SharedPreferences prefs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(mBroadcastReceiveAction);

        setupAlarm();

        prefs = getSharedPreferences("com.firemounta1n.alfabanktest", MODE_PRIVATE);

        titles = getResources().getStringArray(R.array.titles);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (prefs.getBoolean("firstrun", true)) {
            Intent serviceIntent = new Intent(this, DownloadRssService.class);
            startService(serviceIntent);
            prefs.edit().putBoolean("firstrun", false).apply();
        } else {
            findViewById(R.id.loading_panel).setVisibility(View.GONE);
        }

        if (savedInstanceState != null) {
            mNavItemId = savedInstanceState.getInt("NAV_ITEM_ID");
            currentPosition = savedInstanceState.getInt("NAV_ITEM_POS");
            setActionBarTitle(currentPosition);
        } else {
            mNavItemId = R.id.nav_news_fragment;
            setActionBarTitle(currentPosition);

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.flContent, new NewsFragment(), "visible_fragment");
            ft.addToBackStack(null);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();
        }

        drawerToggle = setupDrawerToggle();
        mDrawer.addDrawerListener(drawerToggle);

        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        nvDrawer.getMenu().findItem(mNavItemId).setChecked(true);
        // Setup drawer view
        setupDrawerContent(nvDrawer);

        getSupportFragmentManager().addOnBackStackChangedListener(
                new FragmentManager.OnBackStackChangedListener() {
                    public void onBackStackChanged() {
                        FragmentManager fragMan = getSupportFragmentManager();
                        Fragment fragment = fragMan.findFragmentByTag("visible_fragment");
                        if (fragment instanceof NewsFragment) {
                            currentPosition = 0;
                            mNavItemId = R.id.nav_news_fragment;
                        }
                        if (fragment instanceof FavouriteFragment) {
                            currentPosition = 1;
                            mNavItemId = R.id.nav_favourite_fragment;
                        }
                        if (fragment instanceof AboutFragment) {
                            currentPosition = 2;
                            mNavItemId = R.id.nav_about_fragment;
                        }
                        setActionBarTitle(currentPosition);
                        nvDrawer.getMenu().findItem(mNavItemId).setChecked(true);
                    }
                }
        );
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (currentPosition == 0) {
            finish();
        } else {
            if (mDrawer.isDrawerOpen(GravityCompat.START)) {
                mDrawer.closeDrawer(GravityCompat.START);
            } else {
                getSupportFragmentManager().popBackStack();
            }
        }
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        setActionBarTitle(currentPosition);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        mNavItemId = menuItem.getItemId();
        Fragment fragment = null;
        Class fragmentClass;
        switch(menuItem.getItemId()) {
            case R.id.nav_news_fragment:
                currentPosition = 0;
                fragmentClass = NewsFragment.class;
                break;
            case R.id.nav_favourite_fragment:
                currentPosition = 1;
                fragmentClass = FavouriteFragment.class;
                break;
            case R.id.nav_about_fragment:
                currentPosition = 2;
                fragmentClass = AboutFragment.class;
                break;
            default:
                currentPosition = 0;
                fragmentClass = NewsFragment.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.flContent, fragment, "visible_fragment");
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        setActionBarTitle(currentPosition);
        // Close the navigation drawer
        mDrawer.closeDrawers();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("NAV_ITEM_POS", currentPosition);
        outState.putInt("NAV_ITEM_ID", mNavItemId);
    }

    private void setActionBarTitle(int position) {
        String title;
        title = titles[position];
        try {
            getSupportActionBar().setTitle(title);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void setupAlarm() {
        Context ctx = getApplicationContext();
        Calendar cal = Calendar.getInstance();
        Intent serviceIntent = new Intent(ctx, DownloadRssService.class);
        AlarmManager am = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
        long interval = 1000 * 60 * 5;

        PendingIntent servicePendingIntent =
                PendingIntent.getService(ctx,
                        0,
                        serviceIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT);
        am.cancel(servicePendingIntent);
        am.setRepeating(
                AlarmManager.RTC,
                cal.getTimeInMillis() + interval,
                interval,
                servicePendingIntent
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mReceiver, mIntentFilter);
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(mBroadcastReceiveAction)) {
                String s = intent.getStringExtra(DownloadRssService.SERVICE_MESSAGE);

                if (s.equals("Success")) {
                    findViewById(R.id.loading_panel).setVisibility(View.GONE);

                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.flContent, new NewsFragment(), "visible_fragment");
                    ft.addToBackStack(null);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();

                    Intent stopIntent = new Intent(MainActivity.this,
                            DownloadRssService.class);
                    stopService(stopIntent);
                }
            }
        }
    };

    @Override
    protected void onPause() {
        unregisterReceiver(mReceiver);
        super.onPause();
    }
}


