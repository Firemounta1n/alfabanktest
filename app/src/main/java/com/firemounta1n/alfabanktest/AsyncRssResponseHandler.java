package com.firemounta1n.alfabanktest;

import org.apache.http.Header;

import java.text.ParseException;

public interface AsyncRssResponseHandler {
    public void onSuccess(RssFeed rssFeed) throws ParseException;
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error);
}
