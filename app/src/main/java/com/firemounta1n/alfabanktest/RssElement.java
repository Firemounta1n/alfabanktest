package com.firemounta1n.alfabanktest;

import android.net.Uri;

public abstract class RssElement {

    void setTitle(String title) {
    }

    void setLink(Uri link) {
    }

    void setDescription(String description) {
    }

    void setLanguage(String language) {
    }

    void setPubDate(String pubDate) {
    }

    void setDocs (Uri docs) {
    }

    void setManagingEditor (String managingEditor) {
    }

    void setWebMaster (String webMaster) {
    }

    void setGUID (Uri guid) {
    }

}
