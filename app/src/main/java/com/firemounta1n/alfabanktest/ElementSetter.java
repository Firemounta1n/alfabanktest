package com.firemounta1n.alfabanktest;

import android.net.Uri;

import org.xml.sax.Attributes;

public enum ElementSetter {
    TITLE("title", new ContentSetter() {
        @Override
        public void set(RssElement element, String value) {
            element.setTitle(value);
        }
    }),
    LINK("link", new ContentSetter() {
        @Override
        public void set(RssElement element, String value) {
            element.setLink(Uri.parse(value));
        }
    }),
    DESCRIPTION("description", new ContentSetter() {

        @Override
        public void set(RssElement element, String value) {
            element.setDescription(value);
        }
    }),
    LANGUAGE("language", new ContentSetter() {
        @Override
        public void set(RssElement element, String value) {
            element.setLanguage(value);
        }
    }),
    PUB_DATE("pubDate", new ContentSetter() {
        @Override
        public void set(RssElement element, String value) {
            element.setPubDate(value);
        }
    }),
    DOCS("docs", new ContentSetter() {
        @Override
        public void set(RssElement element, String value) {
            element.setDocs(Uri.parse(value));
        }
    }),
    MANAGINGEDITOR("managingEditor", new ContentSetter() {
        @Override
        public void set(RssElement element, String value) {
            element.setManagingEditor(value);
        }
    }),
    WEBMASTER("webMaster", new ContentSetter() {
        @Override
        public void set(RssElement element, String value) {
            element.setWebMaster(value);
        }
    }),
    GUID("guid", new ContentSetter() {
        @Override
        public void set(RssElement element, String value) {
            element.setGUID(Uri.parse(value));
        }
    });

    private String mQName;

    private Setter mSetter;

    private ElementSetter(String qName, Setter setter) {
        mQName = qName;
        mSetter = setter;
    }

    private static Setter getSetter(String qName) {
        for (ElementSetter elementSetter : ElementSetter.values()) {
            if (elementSetter.mQName.equals(qName)) {
                return elementSetter.mSetter;
            }
        }
        return null;
    }

    public static void setContent(String qName, RssElement element, String content) {
        Setter setter = getSetter(qName);
        if (setter instanceof ContentSetter) {
            ((ContentSetter) setter).set(element, content);
        }
    }

    public static void setAttributes(String qName, RssElement element, Attributes attributes) {
        Setter setter = getSetter(qName);
        if (setter instanceof AttributeSetter) {
            ((AttributeSetter) setter).set(element, attributes);
        }
    }

    public static boolean contains(String qName) {
        return (getSetter(qName) != null);
    }

    public static boolean containsInAttributes(String qName) {
        return (getSetter(qName) instanceof AttributeSetter);
    }

    public static interface Setter {}

    public static interface ContentSetter extends Setter {
        void set(RssElement element, String value);
    }

    private static interface AttributeSetter extends Setter {
        void set(RssElement element, Attributes attributes);
    }

}
