package com.firemounta1n.alfabanktest;

import android.net.Uri;

public class RssItem extends RssElement {

    private String title;
    private Uri link;
    private String description;
    private String pubDate;
    private Uri guid;

    void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    void setLink(Uri link) {
        this.link = link;
    }

    public Uri getLink() {
        return link;
    }

    void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getPubDate() {
        return pubDate;
    }

    void setGUID(Uri guid) {
        this.guid = guid;
    }

    public Uri getGUID() {
        return guid;
    }

}
