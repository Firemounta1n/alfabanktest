package com.firemounta1n.alfabanktest;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class NewsFragment extends Fragment {

    private RssDatabaseHelper dbHelper;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mNewsRecycler;

    private static ArrayList<String> mRSSLink;
    private static ArrayList<String> mRSSGUID;
    private static ArrayList<String> mRSSTitle;
    private static ArrayList<String> mRSSPubDate;
    private static ArrayList<String> mRSSDescription;

    public NewsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ConstraintLayout mConstraintLayout = (ConstraintLayout)
                inflater.inflate(R.layout.fragment_news, container, false);

        mSwipeRefreshLayout = (SwipeRefreshLayout)
                mConstraintLayout.findViewById(R.id.swipe_refresh_layout);

        mNewsRecycler = (RecyclerView)
                mConstraintLayout.findViewById(R.id.news_recycler);

        mNewsRecycler.setHasFixedSize(true);

        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        mNewsRecycler.setLayoutManager(mLayoutManager);

        dbHelper = RssDatabaseHelper.getInstance(getActivity());
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final Cursor c = db.query("ITEMS", null, null, null, null, null, "PUBDATE DESC");

        if (c.moveToFirst()) {

            int rssTitleColIndex = c.getColumnIndex("TITLE");
            int rssPubDateColIndex = c.getColumnIndex("PUBDATE");
            int rssDescriptionColIndex = c.getColumnIndex("DESCRIPTION");
            int rssLinkColIndex = c.getColumnIndex("LINK");
            int rssGUIDColIndex = c.getColumnIndex("GUID");

            mRSSTitle = new ArrayList<>();
            mRSSPubDate = new ArrayList<>();
            mRSSDescription = new ArrayList<>();
            mRSSLink = new ArrayList<>();
            mRSSGUID = new ArrayList<>();

            do {
                try {
                    mRSSTitle.add(c.getString(rssTitleColIndex));
                    mRSSPubDate.add(convertEpoch(c.getLong(rssPubDateColIndex)));
                    mRSSDescription.add(c.getString(rssDescriptionColIndex));
                    mRSSLink.add(c.getString(rssLinkColIndex));
                    mRSSGUID.add(c.getString(rssGUIDColIndex));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } while (c.moveToNext());

            NewsRecycleAdapter mAdapter =
                    new NewsRecycleAdapter(mRSSTitle, mRSSPubDate, mRSSDescription);
            mNewsRecycler.setAdapter(mAdapter);

            mAdapter.setListener(new NewsRecycleAdapter.Listener() {
                public void onClick(int position) {
                    Intent intent = new Intent(getActivity(), WebViewActivity.class);
                    intent.putExtra("RSS_GUID", mRSSGUID.get(position));
                    startActivity(intent);
                }
            });
        }

        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {

                    @Override
                    public void onRefresh() {
                        Intent serviceIntent = new Intent(getActivity(), DownloadRssService.class);
                        getActivity().startService(serviceIntent);

                        mSwipeRefreshLayout.setRefreshing(false);
                        getActivity().stopService(serviceIntent);
                    }
                }
        );

        return mConstraintLayout;
    }

    private String convertEpoch(long epoch) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.ENGLISH);
        String newPubDate = df.format(epoch);
        return newPubDate;
    }
}
