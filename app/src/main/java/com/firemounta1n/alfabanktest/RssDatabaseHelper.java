package com.firemounta1n.alfabanktest;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class RssDatabaseHelper extends SQLiteOpenHelper {

    private static RssDatabaseHelper sInstance;

    private static final String DB_NAME = "rssNews";
    private static final int DB_VERSION = 1;

    static synchronized RssDatabaseHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new RssDatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    public RssDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE ITEMS ("
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "TITLE TEXT, "
                + "LINK TEXT, "
                + "DESCRIPTION TEXT, "
                + "PUBDATE INTEGER, "
                + "GUID TEXT);");

        db.execSQL("CREATE TABLE FAVOURITES ("
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "GUID TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
