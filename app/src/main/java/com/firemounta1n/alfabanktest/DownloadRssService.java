package com.firemounta1n.alfabanktest;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.IBinder;

import org.apache.http.Header;
import org.jsoup.Jsoup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DownloadRssService extends Service {

    static final public String SERVICE_MESSAGE = "MSG";

    RssDatabaseHelper dbHelper;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Context ctx = getApplicationContext();
        dbHelper = RssDatabaseHelper.getInstance(ctx);

        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        final AsyncRssClient client = new AsyncRssClient();
        client.read("http://alfabank.ru/_/rss/_rss.html", new AsyncRssResponseHandler() {

            @Override
            public void onSuccess(RssFeed rssFeed) throws ParseException {
                for (int i = 0; i < rssFeed.getRssItems().size(); i++) {
                    RssItem rssItem = rssFeed.getRssItems().get(i);
                    if (!isItemExists(db, rssItem.getGUID().toString())) {
                        insertItem(db,
                                rssItem.getTitle(),
                                rssItem.getLink().toString(),
                                convertDescription(rssItem.getDescription().substring(0, 500)),
                                convertPubDate(rssItem.getPubDate()),
                                rssItem.getGUID().toString());

                    }
                }
                System.out.println("Success");
                sendResult("Success");
                stopSelf();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody,
                                  Throwable error) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        // Actions to do after 5 seconds
                        Intent serviceIntent = new Intent(DownloadRssService.this,
                                DownloadRssService.class);
                        startService(serviceIntent);
                    }
                }, 5000);
                System.out.println("Failure");
                stopSelf();
            }
        });

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
    }

    public void insertItem(SQLiteDatabase db, String itemTitle, String itemLink,
                                      String itemDescription, long itemPubDate, String itemGUID) {
        ContentValues requestValues = new ContentValues();

        requestValues.put("TITLE", itemTitle);
        requestValues.put("LINK", itemLink);
        requestValues.put("DESCRIPTION", itemDescription);
        requestValues.put("PUBDATE", itemPubDate);
        requestValues.put("GUID", itemGUID);

        db.insert("ITEMS", null, requestValues);

    }

    public boolean isItemExists(SQLiteDatabase db, String itemGUID) {
        Cursor cursor = db.rawQuery("SELECT 1 FROM " + "ITEMS"
                + " WHERE GUID = '" + itemGUID + "'", new String[] {});
        boolean exists = (cursor.getCount() > 0);
        cursor.close();
        return exists;
    }

    public long convertPubDate(String pubDate) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);
        Date date = df.parse(pubDate);
        long epochPubDate = date.getTime();
        return epochPubDate;
    }

    public static String convertDescription(String html) {
        return Jsoup.parse(html).text();
    }

    public void sendResult(String message) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(MainActivity.mBroadcastReceiveAction);
        broadcastIntent.putExtra(SERVICE_MESSAGE, message);
        sendBroadcast(broadcastIntent);
    }
}
