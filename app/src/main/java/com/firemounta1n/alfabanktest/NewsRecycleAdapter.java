package com.firemounta1n.alfabanktest;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class NewsRecycleAdapter extends RecyclerView.Adapter<NewsRecycleAdapter.ViewHolder> {
    private Listener listener;
    private ArrayList mTitles;
    private ArrayList mPubDates;
    private ArrayList mDescriptions;

    public static interface Listener {
        public void onClick(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CardView mCardView;
        public ViewHolder(CardView v) {
            super(v);
            mCardView = v;
        }
    }

    public NewsRecycleAdapter(ArrayList newsTitles, ArrayList newsPubDates,
                              ArrayList newsDescriptions) {
        this.mTitles = newsTitles;
        this.mPubDates = newsPubDates;
        this.mDescriptions = newsDescriptions;
    }

    public void setListener(Listener listener){
        this.listener = listener;
    }

    @Override
    public NewsRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_news_item, parent, false);

        ViewHolder vh = new ViewHolder(cv);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        CardView cardView = holder.mCardView;

        TextView titleText = (TextView) cardView.findViewById(R.id.item_title);
        TextView pubDateText = (TextView) cardView.findViewById(R.id.item_pubDate);
        TextView DescriptionText = (TextView) cardView.findViewById(R.id.item_description);

        titleText.setText(mTitles.get(position).toString());
        pubDateText.setText(mPubDates.get(position).toString());
        DescriptionText.setText(mDescriptions.get(position).toString());

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(holder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mTitles != null) {
            return mTitles.size();
        } return 0;
    }
}
