package com.firemounta1n.alfabanktest;

import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

public class RssFeed extends RssElement {

    private String title;
    private Uri link;
    private String description;
    private String language;
    private String pubDate;
    private Uri docs;
    private String managingEditor;
    private String webMaster;
    private List<RssItem> rssItems;

    void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    void setLink(Uri link) {
        this.link = link;
    }

    public Uri getLink() {
        return link;
    }

    void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getPubDate() {
        return pubDate;
    }

    void setDocs(Uri docs) {
        this.docs = docs;
    }

    public Uri getDocs() {
        return docs;
    }

    void setManagingEditor(String managingEditor) {
        this.managingEditor = managingEditor;
    }

    public String getManagingEditor() {
        return managingEditor;
    }

    void setWebMaster(String webMaster) {
        this.webMaster = webMaster;
    }

    public String getWebMaster() {
        return webMaster;
    }

    void addRssItem(RssItem rssItem) {
        if (rssItems == null) {
            rssItems = new ArrayList<RssItem>();
        }
        rssItems.add(rssItem);
    }

    public List<RssItem> getRssItems() {
        return rssItems;
    }
}
