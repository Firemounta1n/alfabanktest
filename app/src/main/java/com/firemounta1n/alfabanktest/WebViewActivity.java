package com.firemounta1n.alfabanktest;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewActivity extends AppCompatActivity {

    private String url;
    private WebView mWebView;
    private Toolbar toolbar;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RssDatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        Intent intent = getIntent();
        url = intent.getStringExtra("RSS_GUID");

        Context ctx = getApplicationContext();
        dbHelper = RssDatabaseHelper.getInstance(ctx);

        getSupportActionBar().setTitle(url);

        mWebView = (WebView) findViewById(R.id.webView);
        mWebView.setWebViewClient(new mWebClient());
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

        mWebView.loadUrl(url);

        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {

                    @Override
                    public void onRefresh() {
                        mWebView.loadUrl(url);
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }
        );
    }

    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        getMenuInflater().inflate(R.menu.menu_web_view, menu);

        if (isItemExists(db, url)) {
            menu.findItem(R.id.toolbar_favourite).setChecked(true);
            menu.findItem(R.id.toolbar_favourite).setIcon(R.drawable.ic_favourite_on);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        switch (item.getItemId()) {
            case android.R.id.home:
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                onBackPressed();
                return true;
            case R.id.toolbar_favourite:
                if (!item.isChecked()) {
                    item.setChecked(true);
                    item.setIcon(R.drawable.ic_favourite_on);

                    insertItem(db, url);
                } else {
                    item.setChecked(false);
                    item.setIcon(R.drawable.ic_drawer_favourite);

                    db.delete("FAVOURITES", "GUID = ?", new String[]{url});
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void insertItem(SQLiteDatabase db, String itemGUID) {
        ContentValues requestValues = new ContentValues();

        requestValues.put("GUID", itemGUID);

        db.insert("FAVOURITES", null, requestValues);
    }

    public boolean isItemExists(SQLiteDatabase db, String itemGUID) {
        Cursor cursor = db.rawQuery("SELECT 1 FROM " + "FAVOURITES"
                + " WHERE GUID = '" + itemGUID + "'", new String[] {});
        boolean exists = (cursor.getCount() > 0);
        cursor.close();
        return exists;
    }

    public void showDataBase(SQLiteDatabase db) {
        String tableString = String.format("Table %s:\n", "FAVOURITES");
        Cursor allRows  = db.rawQuery("SELECT * FROM " + "FAVOURITES", null);
        if (allRows.moveToFirst()){
            String[] columnNames = allRows.getColumnNames();
            do {
                for (String name: columnNames) {
                    tableString += String.format("%s: %s\n", name,
                            allRows.getString(allRows.getColumnIndex(name)));
                }
                tableString += "\n";

            } while (allRows.moveToNext());
        }
        System.out.println(tableString);
    }

    public class mWebClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }
    }

}
