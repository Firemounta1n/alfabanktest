package com.firemounta1n.alfabanktest;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class FavouriteFragment extends Fragment {

    private RssDatabaseHelper dbHelper;
    private RecyclerView mNewsRecycler;
    private NewsRecycleAdapter mAdapter;

    private ArrayList<String> mRSSLink;
    private ArrayList<String> mRSSGUID;
    private ArrayList<String> mRSSTitle;
    private ArrayList<String> mRSSPubDate;
    private ArrayList<String> mRSSDescription;

    public FavouriteFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ConstraintLayout mConstraintLayout = (ConstraintLayout)
                inflater.inflate(R.layout.fragment_favourite, container, false);

        mNewsRecycler = (RecyclerView)
                mConstraintLayout.findViewById(R.id.news_recycler);

        mNewsRecycler.setHasFixedSize(true);

        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        mNewsRecycler.setLayoutManager(mLayoutManager);

        return mConstraintLayout;
    }

    @Override
    public void onResume() {
        super.onResume();

        reloadDataBase();

        mAdapter = new NewsRecycleAdapter(mRSSTitle, mRSSPubDate, mRSSDescription);
        mNewsRecycler.setAdapter(mAdapter);

        mAdapter.setListener(new NewsRecycleAdapter.Listener() {
            public void onClick(int position) {
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra("RSS_GUID", mRSSGUID.get(position));
                startActivity(intent);
            }
        });
    }

    
    private void reloadDataBase() {

        dbHelper = RssDatabaseHelper.getInstance(getActivity());
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final Cursor c = db.rawQuery(
                "SELECT i.TITLE, i.PUBDATE, i.DESCRIPTION, i.LINK, i.GUID " +
                "FROM " + "ITEMS i " +
                "INNER JOIN FAVOURITES f ON i.GUID = f.GUID", null);

        mRSSTitle = new ArrayList<>();
        mRSSPubDate = new ArrayList<>();
        mRSSDescription = new ArrayList<>();
        mRSSLink = new ArrayList<>();
        mRSSGUID = new ArrayList<>();

        if (c.moveToFirst()) {

            int rssTitleColIndex = c.getColumnIndex("TITLE");
            int rssPubDateColIndex = c.getColumnIndex("PUBDATE");
            int rssDescriptionColIndex = c.getColumnIndex("DESCRIPTION");
            int rssLinkColIndex = c.getColumnIndex("LINK");
            int rssGUIDColIndex = c.getColumnIndex("GUID");

            do {
                try {
                    mRSSTitle.add(c.getString(rssTitleColIndex));
                    mRSSPubDate.add(convertEpoch(c.getLong(rssPubDateColIndex)));
                    mRSSDescription.add(c.getString(rssDescriptionColIndex));
                    mRSSLink.add(c.getString(rssLinkColIndex));
                    mRSSGUID.add(c.getString(rssGUIDColIndex));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } while (c.moveToNext());
        }
    }

    private String convertEpoch(long epoch) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.ENGLISH);
        String newPubDate = df.format(epoch);
        return newPubDate;
    }
}
