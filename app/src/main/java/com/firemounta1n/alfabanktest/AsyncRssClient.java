package com.firemounta1n.alfabanktest;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

public class AsyncRssClient {

    private static final AsyncHttpClient sClient = new AsyncHttpClient();

    private static final RssParser sHandler = new RssParser();

    public void read(String url, final AsyncRssResponseHandler handler) {
        sClient.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    RssFeed rssFeed = sHandler.parse(responseBody);
                    handler.onSuccess(rssFeed);
                } catch (Exception error) {
                    handler.onFailure(statusCode, headers, responseBody, error);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                handler.onFailure(statusCode, headers, responseBody, error);
            }

        });
    }
}